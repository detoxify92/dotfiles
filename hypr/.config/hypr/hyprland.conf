
# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=eDP-1,preferred,auto,auto
monitor=DP-1,2560x1440@75,auto,auto


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox
exec-once = hyprpaper & mpd & mpDris2 
#exec-once = eww open bar & sh ~/.config/eww/scripts/mpdinfo & sh ~/.config/eww/scripts/ewwvars
#exec-once = env XDG_CACHE_HOME=/tmp eww open bar
exec-once = waybar & sh ~/.config/waybar/scripts/mpris_art
exec-once = gnome-keyring-daemon --start --components=pkcs11,secrets,ssh

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORM,wayland
env = QT_QPA_PLATFORMTHEME,flatpak
env = GTK_THEME, Adwaita-dark
env = EDITOR, vim
env = SSH_AUTH_SOCK
env = _JAVA_AWT_WM_NONREPARENTING,1

layerrule = blur,swaync-notification-window
layerrule = blur,waybar
layerrule = ignorezero, waybar
layerrule = ignorezero, bar

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us,es
    kb_options = ctrl:nocaps, altwin:swap_lalt_lwin
    follow_mouse = 2
    touchpad {
        natural_scroll = yes
        clickfinger_behavior = true
    }
    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 3
    gaps_out = 6
    border_size = 1
    col.active_border = rgba(d8a657ff)
    col.inactive_border = rgba(595959aa)
    layout = master
}

misc {
    disable_splash_rendering = true
    disable_hyprland_logo = true
    mouse_move_enables_dpms = true
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 4

    drop_shadow = yes
    shadow_range = 4
    shadow_render_power = 2
    shadow_ignore_window = true
    #col.shadow = rgba(d8a65744)
    col.shadow = rgba(1a1a1aee)
    col.shadow_inactive = rgba(1a1a1aee)

    blur {
        enabled = yes
        size = 18
        passes = 4
        new_optimizations = on
    }

}


animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = overshoot, 0.05, 0.9, 0.1, 1.05


    animation = windows, 1, 5, overshoot, slide
    animation = windowsOut, 1, 5, default, slide
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 5, default
    animation = workspaces, 1, 2, default, slide
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = false
    mfact = 0.55
    inherit_fullscreen = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = off
}


# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2 = float,class:^(term_float)$
windowrulev2 = float,class:^(org.gnome.Calendar)$
windowrulev2 = float,class:^(xdg-desktop-portal-gtk)$

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = WIN_L
$ctrlOnlyMod = CTRL_L
$shiftMod = WIN_L_SHIFT
$ctrlMod =  WIN_L_CTRL_L
$allMod =  WIN_L_SHIFT_CTRL_L

# Misc binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, alacritty
bind = $ctrlMod, RETURN, exec, alacritty --class=term_float,term_float
bind = $shiftMod, C, killactive, 
bind = $shiftMod, Q, exit, 
bind = $shiftMod, D, exec, sleep 1 && hyprctl dispatch dpms off 
bind = $mainMod, F, togglefloating, 
bind = $mainMod, P, exec, tofi-run | sh
bind = $shiftMod, P, exec, passmenu
bind = $ctrlMod, P, exec, ~/.local/scripts/external_display.sh
bind = $allMod, P, exec, ~/.local/scripts/external_display.sh restore
bind = $ctrlOnlyMod, SPACE, exec, dunstctl close-all
bind = ,Print, exec, slurp | grim -g - - | wl-copy
bind = $shiftMod,x, exec, slurp | grim -g - - | wl-copy
bind = $ctrlMod,x, exec, networkmanager_dmenu
bind = $mainMod,Print, exec, grim ~/Pictures/screenshots/$(date +%F-%N).png
bind = $ctrlOnlyMod,Print, exec, hyprctl -j activewindow | jq -r '"\(.at[0]),\(.at[1]) \(.size[0])x\(.size[1])"' | grim -g - - | wl-copy
bind = $shiftMod,s, exec, systemctl suspend
bind = $mainMod,SPACE, exec, ~/.local/scripts/change_keyboard_layout.sh
bind = $mainMod,o, exec, ~/.local/scripts/mount.sh
bind = $shiftMod,o, exec, ~/.local/scripts/umount.sh
bind = $ctrlMod, T, exec, ~/.local/scripts/switch_theme.sh

# Launch apps
bind = $shiftMod, W, exec, firefox
bind = $ctrlMod, W, exec, alacritty -e newsboat
bind = $shiftMod, T, exec, telegram-desktop
bind = $shiftMod, F, exec, ~/.local/scripts/vifm.sh
bind = $shiftMod, N, exec, ~/.local/scripts/ncmpcpp.sh
bind = $ctrlMod, N, exec, feishin --ozone-platform-hint=wayland --enable-features=WaylandWindowDecorations
bind = $shiftMod, up, exec, ~/.local/scripts/change_font_size.sh -i 1
bind = $shiftMod, down, exec, ~/.local/scripts/change_font_size.sh -i -1
bind = $shiftMod, M, exec, alacritty -e neomutt
bind = $ctrlMod, M, exec, ~/.local/scripts/check_mail.sh
bind = $shiftMod, H, exec, alacritty --class=term_float,term_float -e htop
bind = $ctrlMod, H, exec, gnome-calendar
bind = $mainMod, E, exec, ~/.local/scripts/connect_pi.sh
bind = $shiftMod, E, exec, ~/.local/scripts/disonnect_pi.sh
bind = $shiftMod, B, exec, ~/.local/scripts/toggle_bluetoothctl.sh
bind = $ctrlMod, j, exec, ~/.local/scripts/change_opacity.sh -i -.1
bind = $ctrlMod, k, exec, ~/.local/scripts/change_opacity.sh -i .1
bind = $shiftMod, l, exec, alacritty --class=term_float,term_float -e pulsemixer
bind = $ctrlMod, l, exec, hyprlock & sleep 3 && hyprctl dispatch dpms

# Master layout bindings
bind = $shiftMod, RETURN, layoutmsg, swapwithmaster
bind = $mainMod, j, layoutmsg, cyclenext
bind = $mainMod, k, layoutmsg, cycleprev
bind = $mainMod, h, layoutmsg, mfact -0.01
bind = $mainMod, l, layoutmsg, mfact +0.01
bind = $mainMod, m, fullscreen, 1

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Volume and music binds
binde =, XF86AudioRaiseVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +2%
binde =, XF86AudioLowerVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -2%
binde = $shiftMod, j, exec, pactl set-sink-volume @DEFAULT_SINK@ -2%
binde = $shiftMod, k, exec, pactl set-sink-volume @DEFAULT_SINK@ +2%
bind = ,XF86AudioMute, exec, pamixer -t
bind = $mainMod, X, exec, playerctl play-pause
bind = $mainMod, C, exec, playerctl next
bind = $mainMod, Z, exec, playerctl previous
bind = ,XF86AudioPlay, exec, playerctl play-pause
bind = ,XF86AudioNext, exec, playerctl next
bind = ,XF86AudioPrev, exec, playerctl previous
bind = $ctrlMod, C, exec, mpc next 
bind = $ctrlMod, Z, exec, mpc prev 
bind = $ctrlMod, X, exec, ~/.local/scripts/play_pause.sh

# Brightness bindings
bind = ,XF86MonBrightnessUp, exec, brightnessctl s +10%
bind = $allMod, k, exec, brightnessctl s +10%
bind = ,XF86MonBrightnessDown, exec, brightnessctl s 10%-
bind = $allMod, j, exec, brightnessctl s 10%-

