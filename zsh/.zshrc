#          _____                    _____                    _____          
#         /\    \                  /\    \                  /\    \         
#        /::\    \                /::\    \                /::\____\        
#        \:::\    \              /::::\    \              /:::/    /        
#         \:::\    \            /::::::\    \            /:::/    /         
#          \:::\    \          /:::/\:::\    \          /:::/    /          
#           \:::\    \        /:::/__\:::\    \        /:::/____/           
#            \:::\    \       \:::\   \:::\    \      /::::\    \           
#             \:::\    \    ___\:::\   \:::\    \    /::::::\    \   _____  
#              \:::\    \  /\   \:::\   \:::\    \  /:::/\:::\    \ /\    \ 
#_______________\:::\____\/::\   \:::\   \:::\____\/:::/  \:::\    /::\____\
#\::::::::::::::::::/    /\:::\   \:::\   \::/    /\::/    \:::\  /:::/    /
# \::::::::::::::::/____/  \:::\   \:::\   \/____/  \/____/ \:::\/:::/    / 
#  \:::\~~~~\~~~~~~         \:::\   \:::\    \               \::::::/    /  
#   \:::\    \               \:::\   \:::\____\               \::::/    /   
#    \:::\    \               \:::\  /:::/    /               /:::/    /    
#     \:::\    \               \:::\/:::/    /               /:::/    /     
#      \:::\    \               \::::::/    /               /:::/    /      
#       \:::\____\               \::::/    /               /:::/    /       
#        \::/    /                \::/    /                \::/    /        
#         \/____/                  \/____/                  \/____/         
#                                                                           
#### Jaime ZSH config ####

# Path settings
typeset -U PATH path
path=("$HOME/.local/bin" "$path[@]")
export PATH

# Prompt settings
#
# Find and set branch name var if in git repository.
function git_branch_name()
{
  branch=$(git branch 2> /dev/null | grep "\*" | cut -d" " -f2)
  if [[ $branch == "" ]];
  then
    :
  else
    echo ' ('$branch')'
  fi
}

# Enable substitution in the prompt.
setopt prompt_subst
autoload -Uz promptinit 
promptinit
PROMPT='%B%F{yellow}%n%F{yellow}@%m%f:%F{white}%B%~%F{green}$(git_branch_name) %B%F{green}%(!.#.$)%f%b '
precmd () { print -Pn "\e]2;%n@%M:%~\a" } # title bar prompt

# History settings
export HISTSIZE=100000
export SAVEHIIST=100000
export HISTFILE=~/.zsh_history
export SAVEHIST=$HISTSIZE
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY

# Completion settings
autoload -Uz compinit up-line-or-beginning-search down-line-or-beginning-search
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true

# Auto cd
setopt auto_cd

# Vi mode
bindkey -v

# keybindings options
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# Clear the line from the cursor position onwards
bindkey "^K" kill-line

# Exit zsh when ^D is pressed, no matter if the line is filled
exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

# Backward search
bindkey '^R' history-incremental-search-backward
bindkey '^S' history-incremental-search-forward

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# History search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# Enviroment variables


# Colorized man pages
# CHANGE FIRST NUMBER PAIR FOR COMMAND AND FLAG COLOR
#
export LESS_TERMCAP_mb=$'\e[1;33m'
export LESS_TERMCAP_md=$'\e[1;33m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;35m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[0;4;36m'

#########################################
# Colorcodes:
# Black       0;30     Dark Gray     1;30
# Red         0;31     Light Red     1;31
# Green       0;32     Light Green   1;32
# Brown       0;33     Yellow        1;33
# Blue        0;34     Light Blue    1;34
# Purple      0;35     Light Purple  1;35
# Cyan        0;36     Light Cyan    1;36
# Light Gray  0;37     White         1;37
#########################################

# Colorized LS
#

export LS_COLORS='rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;40:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:';

# Aliases
alias ls='ls -hF --color=auto'                        # Colorize output
alias ll='ls -l'                                      # List files in columns
alias la='ll -A'                                      # List also hidden
alias lx='ll -BX'                                     # sort by extension
alias lz='ll -rS'                                     # sort by size
alias lt='ll -rt'                                     # sort by date
alias lm='la | more'                                  # feed list into more
alias get_idf='. $HOME/esp/esp-idf/export.sh'         # set up envirnment for esp32 dev
alias dx='deemix -b 128'                             # short version for download from deezer
alias gs='git status'                                 # short version for git usage
alias gad='git add'                                # short version for git usage
alias gap='git add -p'                                # short version for git usage
alias gip='git push'                                # short version for git usage
alias giff='git diff'                                # short version for git usage
alias gic='git commit'                                # short version for git usage
alias gich='git checkout'                                # short version for git usage
alias gil='git log --graph'                                # short version for git usage
alias gilp='git log -p'                                # short version for git usage
alias nt='alacritty & disown'                         # spawn a new terminal in current directory
alias web='cd ~/HomeCloud/website_hugo_scratch'       # cd to web dir
alias vim='nvim'       # alias for nvim

