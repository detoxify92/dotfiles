" this is mostly a matter of taste. but LaTeX looks good with just a bit

" of indentation.

set sw=2

" TIP: if you write your \label's as \label{fig:something}, then if you

" type in \ref{fig: and press <C-n> you will automatically cycle through

" all the figure labels. Very useful!

set iskeyword+=:

call IMAP('NOM', '\nomenclature{<++>}{<++>}<++>', 'tex')
call IMAP('FIG', 'Figura~\ref{fig:<++>}<++>', 'tex')
call IMAP('FEG', 'Figure~\ref{fig:<++>}<++>', 'tex')
call IMAP('TAB', 'Tabla~\ref{tab:<++>}<++>', 'tex')
call IMAP('TEB', 'Table~\ref{tab:<++>}<++>', 'tex')
call IMAP('COR', 'Código~\ref{list:<++>}<++>', 'tex')
call IMAP('SEC', 'Section~\ref{sec:<++>}<++>', 'tex')
call IMAP('EQ', '(\ref{eq:<++>})<++>', 'tex')
call IMAP('BF', '\textbf{<++>}<++>', 'tex')
call IMAP('IF', '\textit{<++>}<++>', 'tex')
call IMAP('COD', '\code{<++>}<++>', 'tex')
call IMAP('COS', '\codesl{<++>}<++>', 'tex')
call IMAP('COM', '\codeml{<++>}{<++>}<++>', 'tex')

" Environment for columns for use with beamer
let g:Tex_Env_columns = "\\begin{columns}\<CR>\\begin{column}{0.5\\textwidth}\<CR><++>\<CR>\\end{column}\<CR>\\begin{column}{0.5\\textwidth}\<CR><++>\<CR>\\end{column}\<CR>\\end{columns}<++>"
let g:Tex_Env_frame = "\\begin{frame}{<++>}\<CR><++>\<CR>\\end{frame}<++>"


" Environment for code listings
let g:Tex_Env_listing = "\\begin{listing}[t]\<CR>\\begin{minted}{<++>}\<CR>\<++>\<CR>\\end{minted}\<CR>\\caption{<++>}\<CR>\\label{list:<++>}\<CR>\\end{listing}\<CR><++>"
let g:Tex_Env_listinp = "\\begin{listing}[t]\<CR>\\inputminted{<++>}{<++>}\<CR>\\caption{<++>}\<CR>\\label{list:<++>}\<CR>\\end{listing}\<CR><++>"

let g:Tex_IgnoredWarnings =
	\'Underfull'."\n".
	\'Overfull'."\n".
	\'specifier changed to'."\n".
	\'You have requested'."\n".
	\'Missing number, treated as zero.'."\n".
	\'LaTeX Font Warning'."\n".
	\'Package spanish Warning'."\n".
	\'There were undefined references'."\n".
	\'Label %.%# multiply defined'."\n".
	\'There were multiply-defined labels'."\n".
   \'Package biblatex Warning: Using fall'."\n".
   \'Package biblatex Warning: Please (re)run'."\n".
	\'option without twoside'."\n".
	\'Citation %.%# undefined'

" the 'ignore level' of the 'efm'. A value of 4 says that the first 4 kinds of
" warnings in the list above will be ignored. Use the command TCLevel to set a
" level dynamically.
let g:Tex_IgnoreLevel = 15


" specifies the MakeIndedx flavor and if necessary options. $* will be
" replaced by the *root* of the main file name. See above.
let g:Tex_MakeIndexFlavor = 'makeindex $*.nlo -s nomencl.ist -o $*.nls'

let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 -interaction=nonstopmode -shell-escape -file-line-error-style $*'

let g:Tex_BibtexFlavor = 'bibtex'
