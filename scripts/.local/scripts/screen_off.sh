#!/bin/sh
# Sleeps 1 s and turn off screen (the delay is necessary so you dont wake the screen while releasing the shortcut key)

sleep 1
xset dpms force off
