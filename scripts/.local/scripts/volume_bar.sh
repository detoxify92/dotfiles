#!/bin/sh
# Shows a volume bar whose width depends on the volume of the
# current sink

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
if [[ $(grep -A 4 'Headphone Playback Switch' /proc/asound/card0/codec\#0 | grep 'Amp-Out vals' | awk ' {print $3" "$4} ') = "[0x80 0x80]" ]]; then
	icon="~/.local/icons/volume.svg"
else
	icon="~/.local/icons/headphones.svg"
fi
eval sh ~/.local/scripts/colorize_icon.sh $icon $color
dunstify -h string:x-dunst-stack-tag:volume -u low -i '/tmp/icon.svg' "$1 volume" " $(sh ~/.local/scripts/progress_bar.sh 30 "-" " " $(pamixer --get-volume))"
canberra-gtk-play -i audio-volume-change
