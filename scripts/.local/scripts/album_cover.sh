#!/bin/bash
# Script that sends a notification for the song playing

# Directory and filepath of the current song 
dir="$(dirname /home/jaime/Music/"$(mpc --format '%file%' current)")"
file="/home/jaime/Music/""$(mpc --format '%file%' current)"
# Directory of album art database and name of file
aadb="/home/jaime/.local/share/aadb/"
track_cover="$(mpc --format '%title%_%artist%' current | iconv -cf UTF-8 -t ASCII//TRANSLIT | tr -d ' [:punct:]' | tr '[:upper:]' '[:lower:]' | tr ' ' '-' | sed "s/-\+/-/g; s/\(^-\|-\$\)//g").jpg"
album_cover="$(mpc --format '%album%' current | iconv -cf UTF-8 -t ASCII//TRANSLIT | tr -d ' [:punct:]' | tr '[:upper:]' '[:lower:]' | tr ' ' '-' | sed "s/-\+/-/g; s/\(^-\|-\$\)//g").jpg"
# Fomatted strings for notification
title=$(mpc --format '%title%' current)
alb_auth=$(mpc --format 'by %artist% \nfrom %album%' current)

# If there is a cover in the local database, use it
if [ -f $aadb$album_cover ]; then
        notification_cover="$aadb$album_cover"
elif [ -f $aadb$track_cover ]; then
        notification_cover="$aadb$track_cover"
else
   # If there is a cover within the directory, use it as album cover
   if [ -f "$dir/cover.jpg" ]; then
        cp "$dir"/cover.jpg $aadb$album_cover
        notification_cover="$aadb$album_cover"
   # If the file has album cover attached, use it
   elif [ -n "$(ffmpeg -i "$file" 2>&1 | grep Video)" ]; then
   	ffmpeg -v quiet -y -i "$file" -an ~/.config/ncmpcpp/tmp.jpg
        cp ~/.config/ncmpcpp/tmp.jpg $aadb$track_cover
        notification_cover="$aadb$track_cover"
   # If there is no cover available, use a default
   else
        notification_cover="~/.config/ncmpcpp/default.jpg"
   fi
fi
# Copy the cover to /tmp for other uses
cp $notification_cover /tmp/notification_cover.jpg
# Launch the notification if not in wayland
if [[ -n $(pgrep "waybar") ]];
then
    pkill -SIGRTMIN+9 waybar
elif [[ -n $(pgrep eww) ]]; then
    sh ~/.config/eww/scripts/music
else
    dunstify -h string:x-dunst-stack-tag:newSong -u low -i "$notification_cover" "$title" "$alb_auth"
fi

