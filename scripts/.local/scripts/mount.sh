#!/bin/sh
# Script that gives a dmenu propmt for mounting usb drives

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/usb.svg $color
# Look for mountable drives
mountable=$(lsblk -lp | awk ' $6 == "part" && $7 =="" {print $1, "(" $4 ")"} ')
# If list is empty, exit
[[ "$mountable" = "" ]] && notify-send -u low --icon='/tmp/icon.svg' 'mount' 'No drive to mount' && exit 1
# Get user selection
prompt="Mount which drive?"
if [[ -n $WAYLAND_DISPLAY ]]; then
        chosen=$(echo "$mountable" | tofi --prompt-text="${prompt}" | awk '{print $1}')
elif [[ -n $DISPLAY ]]; then
        chosen=$(echo "$mountable" | dmenu -i -p "${prompt}" | awk '{print $1}')
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi
# If selection is empty, exit
[[ "$chosen" = "" ]] && exit 1
# Get mount directories
dirs=$(find /mnt/* -type d -maxdepth 1 2>/dev/null)
# Get user selection
prompt="Select mountpoint"
if [[ -n $WAYLAND_DISPLAY ]]; then
        mountpoint=$(echo "$dirs" | tofi --prompt-text="${prompt}")
elif [[ -n $DISPLAY ]]; then
        mountpoint=$(echo "$dirs" | dmenu -i -p "${prompt}")
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi
# Check if null input
[[ "$mountpoint" = "" ]] && exit 1
# Try to mount
if sudo mount -o gid=users,fmask=113,dmask=002 $chosen $mountpoint
then
	notify-send -u low --icon='/tmp/icon.svg' 'mount' "$chosen mounted to $mountpoint"
elif sudo mount $chosen $mountpoint
then
	notify-send -u low --icon='/tmp/icon.svg' 'mount' "$chosen mounted to $mountpoint without adecuate permissions"
else
   eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/warning.svg $color
	notify-send -u low --icon='/tmp/icon.svg' 'mount' "Could not mount $chosen"
fi
