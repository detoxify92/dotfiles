#!/bin/sh
# Scripts that launches a paddingless alacritty with vifm
alacritty --config-file ~/.config/alacritty/alacritty_np.toml --class=vifm,vifm -e vifm
