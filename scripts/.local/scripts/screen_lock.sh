#!/bin/sh
# Locks, sleeps 1 s and turn off screen (the delay is necessary so you dont wake the screen while releasing the shortcut key)

slock &
sleep 1
xset dpms force off
