#!/bin/sh
# Preppend track number to filename. Adds 0s according to parameter and track
# number

# Try to get the track key
[ ! -z "$(vorbiscomment -l "${1}" | grep -i TRACK)" ] && track_key="TRACK"
[ ! -z "$(vorbiscomment -l "${1}" | grep -i TRACKNUMBER)" ] && track_key="TRACKNUMBER"
# If no track information availaible, end
[ -z $track_key ] && echo "No track information found" && exit 1
# Get the track number
if [ ! -z "$(vorbiscomment -l "${1}" | grep $track_key)" ]
then
   track=$(vorbiscomment -l "${1}" | grep $track_key| cut -d= -f 2)
elif [ ! -z "$(vorbiscomment -l "${1}" | grep track)" ]
then
   track=$(vorbiscomment -l "${1}" | grep track | cut -d= -f 2)
fi 
# Compute number of zeroes as the number given as paramter minus the number of
# digits in the tracknumber
[ "${track}" -ge 10 ] && number_zeroes=$(("${2}"-2)) || number_zeroes=$(("${2}"-1))
if [ $number_zeroes -gt 0 ]
then
    filename_prefix=$(printf '0%.0s' $(seq 1 $number_zeroes))
else 
    filename_prefix=""
fi
# Change file name
mv "${1}" "${filename_prefix}${track}_$1"
