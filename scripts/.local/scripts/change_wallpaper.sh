#!/bin/bash
# Script that displays available wallpapers inside ~/Pictures/Wallpapers and
# applies selected with feh

wallpaper=$(ls -l ~/Pictures/Wallpapers | awk 'NR!=1 {print $9}' | dmenu -i -p 'Select wallpaper:')
[[ -z $wallpaper ]] && echo "Empty selection" && exit 1

feh --bg-scale ~/Pictures/Wallpapers/"${wallpaper}"
