#!/bin/sh
# Check for new mail with mbsync every 10 minutes
# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
# Get icon for notification and colorize
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/down_email.svg $color
# If the script is already running in background just download and exit
if [[ $$ != $(pgrep -f /scripts/check_mail.sh | head -n 1) ]]; then
    # Notify that download is starting
    dunstify -h string:x-dunst-stack-tag:downEmail \
    -i "/tmp/icon.svg" "isync" "Downloading new email and exiting..."
    # Download email
    mbsync -a > /dev/null
    exit 0
else
    # Notify that download is starting
    dunstify -h string:x-dunst-stack-tag:downEmail \
    -i "/tmp/icon.svg" "isync" "Downloading new email..."
    while :
    do
       # Download email
       mbsync -a > /dev/null
       sleep 10m
    done
fi

# These can be used to send a notification each time the email downloading is activated

# Launch a notification while downloading
#dunstify -h string:x-dunst-stack-tag:downEmail -u critical \
#   -i "/tmp/icon.svg" "isync" "Downloading new email..."
# Launch a notification when done
#dunstify -h string:x-dunst-stack-tag:downEmail -i "/tmp/icon.svg" "isync" "Download finished"
