#!/bin/bash

for keyboard in $(hyprctl devices | grep keyboard); do
    hyprctl switchxkblayout $keyboard next
done
pkill -SIGRTMIN+8 waybar
