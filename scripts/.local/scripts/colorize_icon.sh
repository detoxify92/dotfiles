#!/bin/sh
# Script that colorizes the icon used for notification with the color given.
# It writes the colorized icon to /tmp/icon.svg
# $1 icon, $2 color

sed "s/ffffff/$2/g" $1 > /tmp/icon.svg

