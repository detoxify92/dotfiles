#!/bin/sh
# Script that gets the foreground color from various config files
# -d for dunst

case $1 in
   "-d")
      printf "%s\n" $(cat ~/.config/dunst/dunstrc| grep 'foreground = "#' | awk 'NR==1 {print $3}' | sed 's/["|#]//g')
   ;;
   *)
      printf "ffffff\n"
   ;;
esac

