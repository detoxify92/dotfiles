#!/bin/sh

# Script that sends a notification for a new email

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/email.svg $color
# Send notification
dunstify -h string:x-dunst-stack-tag:newEmail -u critical --icon='/tmp/icon.svg' 'neomutt' 'You have new mail.' 
