#!/bin/bash
# Script that handles external monitors with wayland (hyprland)

### Variables
internal_display="eDP-1"
internal_display_bar_width="1432"

# Get the external display, exit if none
external_display=$(hyprctl monitors | grep Monitor | grep -v eDP | awk '{print $2}')
external_display_bar_width="2552"

if [[ "$1" == "restore" ]]; then
    echo "Restaurando"
    hyprctl keyword monitor "$external_display, disable"
    hyprctl keyword monitor "$internal_display, preferred, auto, 2"
    sed -i "/^width/ s/=.*/= $internal_display_bar_width/" .config/tofi/config
    exit 0
else
    echo "Unrecognized command"
fi

if [[ -n $(hyprctl monitors | grep $internal_display) ]]; then
    [[ -z $external_display ]] && echo "No external display connected" && exit 1
    hyprctl keyword monitor "$internal_display, disable"
    sed -i "/^width/ s/=.*/= ${external_display_bar_width}/" .config/tofi/config
else
    hyprctl keyword monitor "$external_display, disable"
    hyprctl keyword monitor "$internal_display, preferred, auto, 2"
    sed -i "/^width/ s/=.*/= $internal_display_bar_width/" .config/tofi/config
fi
