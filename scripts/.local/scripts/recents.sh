#!/bin/sh
# Launch a dmenu with recent edited documents to edit

# Get the file path to edit and output to dmenu for the user to select
path2file=$(awk ' /\47[0-9]/  {print $4} ' ~/.viminfo | dmenu -p 'Edit recent file:' | tr -d '\n')

if [ -z $path2file ] 
then
	# Exit the script with fail code
	printf 'Empty selection\n'
	exit 1
else
	# Launch a terminal with the file opened in vim
	eval alacritty -e vim -o "$path2file"
	exit 0
fi
