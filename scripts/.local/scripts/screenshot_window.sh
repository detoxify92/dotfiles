#!/bin/sh
# Script that copies a window screenshot to clipboard and sends a notification

# Take screenshot and copy to clipboard
maim -B -u -i $(xdotool getactivewindow) | xclip -selection clipboard -t image/png -i 
# Save icon for notification
xclip -selection clipboard -o > /tmp/sh.png
# Send notification
notify-send -u low --icon='/tmp/sh.png' 'maim' 'Screenshot copied to clipboard'
