#!/bin/bash
# Script that handles external monitors
# Variables with the names of the displays
intern=eDP-1
extern=HDMI-1

if xrandr | grep "$extern disconnected"; then
    xrandr --output "$extern" --off --output "$intern" --auto
else
    if [[ $# -eq 0 ]]; then
	    xrandr --output "$intern" --off --output "$extern" --auto
    else
    case $1 in
		"-hr")
			echo "HD ready"
            xrandr --output "$intern" --off --output "$extern" --mode 1280x720
			;;
		"-er")
			echo "Extender derecha"
            xrandr --output "$extern" --auto --right-of "$intern"
			;;
		"-el")
			echo "Extender izquierda"
            xrandr --output "$extern" --auto --left-of "$intern"
			;;
		"-h")
			echo "Script to handle external displays connected to a laptop"
            echo "If no command is given, turns of laptop screen and uses external display"
            echo "If the external display is disconnected, the laptop screen will be used"
            echo "Sintax: toggle_monitor <options>"
            echo "Options"
            echo "-hr     Use external 720p display"
            echo "-er     Right extend"
            echo "-el     Left extend"
            echo "-h      Show this help message"

			;;
		*)
			echo "Not recognized, use h for help"
	esac
    fi
fi
