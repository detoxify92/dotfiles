#!/bin/bash
# Script that display bluetooth paired devices and connects to the selected one

prompt="Select bluetooth device:"
rfkill unblock bluetooth
if [[ -n $WAYLAND_DISPLAY ]]; then
        bdevice=$(bluetoothctl devices | cut -d' ' -f3-| tofi --prompt-text="${prompt}")
elif [[ -n $DISPLAY ]]; then
        bdevice=$(bluetoothctl devices | cut -d' ' -f3-| dmenu -i -p 'Select bluetooth device:')
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi

bluetoothctl connect $(bluetoothctl devices | awk -v regex="$bdevice" '$0 ~ regex {print $2}')
