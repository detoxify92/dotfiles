#!/bin/sh
# Script that copies a screenshot of a screen region to clipboard and sends a notification

maim -s -u | xclip -selection clipboard -t image/png -i 
xclip -selection clipboard -o > /tmp/sh.png
notify-send -u low --icon='/tmp/sh.png' 'maim' 'Screenshot copied to clipboard'
