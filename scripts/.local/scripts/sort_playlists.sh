#!/bin/sh
# Sort mpd playlists

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/playlist.svg $color

# Get the date, filename and path
find ~/Music/Remote/playlists -type f -iname '*.m3u' -exec sort '{}' -o '{}' \;

# Send notification
notify-send -u low --icon='/tmp/icon.svg' 'ncmpcpp' "Playlists sorted"
