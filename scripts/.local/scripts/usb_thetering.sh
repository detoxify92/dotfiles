#!/bin/sh

# Script that sends a notification for used data over usb thetering

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/usb_the.svg $color
# Look if usb thetering is active
interface=$(cat /proc/net/dev | grep u1u2)
# If list is empty, exit
[[ "$interface" = "" ]] && notify-send -u low --icon='/tmp/icon.svg' 'USB thetering' 'Interface not detected' && exit 1
# Get used data
data=$(echo $interface | awk '{print $2/2^20 " MiB"}')
# Send notification
notify-send -u low --icon='/tmp/icon.svg' 'USB thetering' "$data used so far"
