#!/bin/bash
# Script that takes a wallpaper as input and sets it as wallpaper using hyprpaper

sed -i --follow-symlinks "/preload/ s|= .*|= $1|" ~/.config/hypr/hyprpaper.conf
sed -i --follow-symlinks "/wallpaper/ s|,.*|,$1|" ~/.config/hypr/hyprpaper.conf

hyprctl hyprpaper unload all
hyprctl hyprpaper preload $1
hyprctl hyprpaper wallpaper LVDS-1,$1
hyprctl hyprpaper wallpaper DP-1,$1
hyprctl hyprpaper wallpaper eDP-1,$1
