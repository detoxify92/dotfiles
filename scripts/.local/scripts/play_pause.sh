#!/bin/bash
# Togle play-pause for mpd

# Check if mpd is running or exit right away
mpc | grep -e playing -e paused || exit 1
# Store the volume
volume_ini=$(mpc status | grep volume | grep -o "[0-9]*")
echo $volume_ini
# Current directory
BASEDIR=$(dirname $0)
# If mpd is paused, resume and increase volume gradually
# Otherwise reduce volume gradually and pause
if [ -n "$(mpc status | grep paused)" ]; then
	mpc volume 0 
	volume=0
	sh $BASEDIR/album_cover.sh
	mpc toggle
	while [ $volume -lt $volume_ini ] 
	do
		let "volume = volume + 5"
		mpc volume $volume
	done
	mpc volume $volume_ini
else
	volume=$volume_ini
	while [ $volume -gt 0 ] 
	do
		let "volume = volume - 5"
		mpc volume $volume
	done
	mpc toggle
	mpc volume $volume_ini
fi
