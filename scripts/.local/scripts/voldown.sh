#!/bin/sh
# Script that decreases volume of default sink
sink=$(ponymix | awk ' NR==2 {print $1} ')
pactl set-sink-volume @DEFAULT_SINK@ -2%
sh ~/.local/scripts/volume_bar.sh $sink
sh ~/.local/scripts/volupd.sh 
