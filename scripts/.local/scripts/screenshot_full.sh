#!/bin/sh
# Save a screenshot of the full desktop to the screenshot folder and sends a notification

# Get the date, filename and path
filename=$(date +%F-%N)
path="~/Pictures/screenshots/$filename.png"
# Save the screenshot
maim ~/Pictures/screenshots/$filename.png
# Send notification
notify-send -u low --icon=$path 'maim' 'Screenshot saved to screenshots folder'
