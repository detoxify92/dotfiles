#!/bin/sh
# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/no-sound.svg $color
# Get the default mixer
sink=$(ponymix | awk ' NR==2 {print $1} ')
# Toggle mute
if [[ $(pamixer --get-mute) = true ]] 
then
	pamixer -u
	sh ~/.local/scripts/volume_bar.sh $sink
else
	pamixer -m
	dunstify -h string:x-dunst-stack-tag:volume -u low -i "/tmp/icon.svg" "pulseaudio" "$sink audio muted"
fi
# Update volume information
sh ~/.local/scripts/volupd.sh 
