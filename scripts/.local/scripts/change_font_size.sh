#!/bin/sh
# Change font size

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/font.svg $color
# Get current value
current_size=$(grep 'size =' ~/.config/alacritty/alacritty.toml | awk ' {print $3} ')
# Change it accordingly to option selected
case $1 in
	"-i")
		echo "Relative change"
      # Check limits
		new_size=$(echo "$current_size + $2" | bc -l)
		if (( $(echo "$new_size < 1" | bc -l) )); then
			new_size="1"
		fi
		if (( $(echo "$new_size > 20" | bc -l) )); then
			new_size="20"
		fi
		echo $new_size
		sed -i --follow-symlinks "/size =/ s/$current_size/$new_size/" ~/.config/alacritty/alacritty.toml
		sed -i --follow-symlinks "/size =/ s/$current_size/$new_size/" ~/.config/alacritty/alacritty_np.toml
                sed -i --follow-symlinks "/font = monospace/ s/monospace.*$/monospace $(($new_size - 1))/" ~/.config/dunst/dunstrc 
                killall dunst 
                nohup dunst &
                dunstify -h string:x-dunst-stack-tag:font -u low -i '/tmp/icon.svg' "System" "Font size set to $new_size"
		;;
	"-a")
		echo "Absolute change"
		sed -i --follow-symlinks "/size:/ s/$current_size/$2/" ~/.config/alacritty/alacritty.toml
		sed -i --follow-symlinks "/size:/ s/$current_size/$2/" ~/.config/alacritty/alacritty_np.toml
		sed -i --follow-symlinks "/font = monospace/ s/monospace.*$/monospace $(($new_size - 1))/" .config/dunst/dunstrc
                killall dunst 
                nohup dunst &
                dunstify -h string:x-dunst-stack-tag:font -u low -i '/tmp/icon.svg' "System" "Font size set to $2"
		;;
esac
