#!/bin/sh
# Edit metadata of the given ogg file

# Check arguments
if [[ $# -lt 3 ]]; then
	echo "Bad usage. Usage:"
        echo "edit_metadata PROPERTY NEW_VALUE FILE"
        exit 1
fi

# Copy metadata to tmp file
echo "Processing the file: $3"
vorbiscomment -l "$3" > /tmp/metadata

# Check if file has property and exits if not
[[ -z $(grep $1 /tmp/metadata) ]] && echo "Property $1 not found in the metadata of the file: $3" && exit 2

# Assign new value to property
echo "Assigning value $2 to property $1"
sed -i "/$1/ s/=.*/=$2/" /tmp/metadata

# Assign metadata to file
vorbiscomment -c /tmp/metadata -we "$3"
