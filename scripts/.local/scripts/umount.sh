#!/bin/sh
# Script that gives a dmenu propmt for umounting usb drives

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/usb.svg $color
# Look for drives that can be safely umounted
drives=$(lsblk -lp | awk '$6 == "part" && $7 !~ /home|boot|SWAP|\/$|^$/ {print $1, "(" $4 ")"}')
# If list is empty, exit
[[ "$drives" = "" ]] && notify-send -u low --icon='/tmp/icon.svg' 'mount' "No drive detected to umount" && exit 1
# Get user selection
prompt="Umount which drive?"
if [[ -n $WAYLAND_DISPLAY ]]; then
        chosen=$(echo "$drives" | tofi --prompt-text="${prompt}" | awk '{print $1}')
elif [[ -n $DISPLAY ]]; then
        chosen=$(echo "$drives" | dmenu -i -p "${prompt}" | awk '{print $1}')
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi
# If selection is empty, exit
[[ "$chosen" = "" ]] && exit 1
# Try to umount
if sudo umount $chosen  
then
	notify-send -u low --icon='/tmp/icon.svg' 'mount' "$chosen successfully umounted"
else
   eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/warning.svg $color
	notify-send -u low --icon='/tmp/icon.svg' 'mount' "Could not umount$chosen, target busy"
fi

