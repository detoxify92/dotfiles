#!/bin/bash
# Script that handles external monitors

### Config file
CONFIG_FILE="$HOME/.config/screen.conf"

### Variables
option1="Use last configuration"
option2="Connect to screen"
option3="Save new display configuration"
option4="Connect to display using saved configuration"
options="$option1\n$option2\n$option3\n$option4"

### Functions
getScreenDetails() {
    xrandrOutput=$(xrandr)
    screenOptions=$(grep -e "connected" -n <<< $xrandrOutput)
    screenOptionsConnected=$(grep -e " connected " <<< $screenOptions )
    screenSelected=$(echo "$screenOptionsConnected" | awk '{print $1}' | awk -F: '{print $2}' | dmenu -p "Select display" -l 5)
    [[ -z $screenSelected ]] && echo "Empty selection" && exit 1
    lineSelected=$(echo "$screenOptionsConnected" | awk '{print $1}' | awk -v selected="$screenSelected" -F: '{if ($2 == selected) print $1}')
    nextLine=$(echo "$screenOptions" | awk -F: -v line="$lineSelected" '{if ($1 > line) print$1}' | head -n 1)
    modesSelected=$(echo "$xrandrOutput" | tail -n +$((lineSelected+1)) | head -n $((nextLine-lineSelected-1)) )
    resolutionSelected=$(echo "$modesSelected" | awk '{print $1}' | dmenu -p "Select resolution")
    [[ -z $resolutionSelected ]] && echo "Empty selection" && exit 1
    rateSelected=$(grep -e $resolutionSelected <<< $modesSelected | awk '{$1=""; print $0}' | grep -o '[0-9|\.]\+' | dmenu -p "Select samplerate")
    otherScreens=$(grep -v $screenSelected <<< $screenOptions| awk '{print $1}' | awk -F: '{print $2}')
    for screen in $otherScreens
    do
        otherScreensCommand="${otherScreensCommand} --output $screen --off "
    done
    xrandrArguments="--output $screenSelected --mode $resolutionSelected --rate $rateSelected ${otherScreensCommand}"
}

connectScreen() {
    echo "Connecting to screen..."
    eval "xrandr ${xrandrArguments}"
    sed -i "/last=/ s/=.*/=${xrandrArguments}/" $CONFIG_FILE
    sh ~/.fehbg
}

saveConfiguration() {
    name="$(dmenu -p "Enter name for display configuration" < /dev/null)"
    echo -e "option_${name}=${xrandrArguments}" >> $CONFIG_FILE
}

loadConfiguration() {
    selectedConfigurationName="$(grep -e "option_" $CONFIG_FILE | awk -F_ '{print $2}' | awk -F= '{print $1}' | dmenu -p "Load which configuration?")"
    [[ -z $selectedConfigurationName ]] && echo "Empty selection" && exit 1
    xrandrArguments="$(grep -e "${selectedConfigurationName}" $CONFIG_FILE | awk -F= '{print $2}')"
}

loadConfigurationFromName() {
    xrandrArguments="$(grep -e "${selectedConfigurationName}" $CONFIG_FILE | awk -F= '{print $2}')"
    [[ -z $xrandrArguments ]] && echo "No configuration with name: ${selectedConfigurationName}" && exit 1
}

getLastConfiguration() {
    xrandrArguments="$(grep -e "last" $CONFIG_FILE | awk -F= '{print $2}')"
}

mainMenu() {    
    echo -e "$options" | dmenu -p "Select an option"
}

checkConfigFile() {
    if [ ! -f "$CONFIG_FILE" ]; then
        echo -e "last=" > $CONFIG_FILE
    fi
}

### Main logic
checkConfigFile
if [[ $# -eq 1 ]]; then
    selectedConfigurationName="${1}"
    loadConfigurationFromName
    echo "$xrandrArguments"
    connectScreen
elif [[ $# -eq 0 ]]; then
    menuOptionSelected=$(mainMenu)
    case "$menuOptionSelected" in
    	$option1) getLastConfiguration && connectScreen ;;
    	$option2) getScreenDetails && connectScreen ;;
    	$option3) getScreenDetails && saveConfiguration ;;
    	$option4) loadConfiguration && connectScreen;;
            *) echo "Empty selection" && exit 1
    esac
else
    echo "Bad usage. Use without arguments for dmenu interactive version. Give name of connection in $CONFIG_FILE for connecting to it" && exit 1
fi

