#!/usr/bin/bash
# Script that chooses pulseaudio sink via dmenu
# Changes default sink and moves all streams to that sink

if [[ -n $WAYLAND_DISPLAY ]]; then
        sink=$(ponymix -t sink list|awk '/^sink/ {s=$1" "$2;getline;gsub(/^ +/,"",$0);print s" "$0}'|tofi --prompt-text='Pulseaudio sink:' |grep -Po '[0-9]+(?=:)')
elif [[ -n $DISPLAY ]]; then
        sink=$(ponymix -t sink list|awk '/^sink/ {s=$1" "$2;getline;gsub(/^ +/,"",$0);print s" "$0}'|dmenu -i -p 'Pulseaudio sink:' |grep -Po '[0-9]+(?=:)') 
else
	echo "Error: No Wayland or X11 display detected" >&2
	exit 1
fi

[[ -z $sink ]] && echo "Invalid selection" && exit 1


ponymix set-default -d $sink &&
for input in $(ponymix list -t sink-input|grep -Po '[0-9]+(?=:)');do
	echo "$input -> $sink"
	ponymix -t sink-input -d $input move $sink
done
# Update volume information
sh ~/.local/scripts/volupd.sh 
