#!/bin/sh
# Script that toggles dark/light mode

# Config variables
GTK_DARK_THEME="Gruvbox-Material-Dark"
GTK_LIGHT_THEME="Gruvbox-Light"
GTK_DARK_ICON_THEME="Gruvbox-Dark"
GTK_LIGHT_ICON_THEME="Gruvbox-Light"
ALACRITY_DARK_THEME="adwaita.toml"
ALACRITY_LIGHT_THEME="adwaita_light.toml"

# Get current mode
current_theme="$(gsettings get org.gnome.desktop.interface color-scheme)"

if [ $current_theme == "'prefer-dark'" ]; then
    echo "Changing theme to light variation"
    gsettings set org.gnome.desktop.interface color-scheme prefer-light
    sed --follow-symlinks -i '/background/ s/"dark"/"light"/' ~/.config/nvim/init.lua
    sed --follow-symlinks -i "/import/ s/$ALACRITY_DARK_THEME/$ALACRITY_LIGHT_THEME/" ~/.config/alacritty/alacritty.toml
    sed --follow-symlinks -i "/import/ s/$ALACRITY_DARK_THEME/$ALACRITY_LIGHT_THEME/" ~/.config/alacritty/alacritty_np.toml
    sed -i --follow-symlinks "/opacity =/ s/= .*/= 1/" $HOME/.config/alacritty/alacritty.toml
    sed -i --follow-symlinks "/opacity =/ s/= .*/= 1/" $HOME/.config/alacritty/alacritty_np.toml
    sed -i --follow-symlinks "/import/ s/dark/light/" .config/waybar/style.css
    killall -SIGUSR2 waybar
    # Send notification
    dunstify -h string:x-dunst-stack-tag:changeTheme "System" "Changed to light theme"
else
    echo "Changing theme to dark variation"
    gsettings set org.gnome.desktop.interface color-scheme prefer-dark
    sed --follow-symlinks -i '/background/ s/"light"/"dark"/' ~/.config/nvim/init.lua
    sed --follow-symlinks -i "/import/ s/$ALACRITY_LIGHT_THEME/$ALACRITY_DARK_THEME/" ~/.config/alacritty/alacritty.toml
    sed --follow-symlinks -i "/import/ s/$ALACRITY_LIGHT_THEME/$ALACRITY_DARK_THEME/" ~/.config/alacritty/alacritty_np.toml
    sed -i --follow-symlinks "/opacity =/ s/= .*/= 1/" $HOME/.config/alacritty/alacritty.toml
    sed -i --follow-symlinks "/opacity =/ s/= .*/= 1/" $HOME/.config/alacritty/alacritty_np.toml
    sed -i --follow-symlinks "/import/ s/light/dark/" .config/waybar/style.css
    killall -SIGUSR2 waybar
    # Send notification
    dunstify -h string:x-dunst-stack-tag:changeTheme "System" "Changed to dark theme"
fi
