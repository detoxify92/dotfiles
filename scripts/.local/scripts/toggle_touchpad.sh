#!/usr/bin/sh
# Script that toggles the touchpad on or off

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
if [[  $(xinput list-props "SynPS/2 Synaptics TouchPad" | awk ' $4==Device {print $4} ') == 0 ]]; then
   xinput enable "SynPS/2 Synaptics TouchPad"
   eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/touchpad.svg $color
	dunstify -h string:x-dunst-stack-tag:touchpad -u low -i "/tmp/icon.svg" "xinput" "Touchpad activated"
else
   xinput disable "SynPS/2 Synaptics TouchPad"
   eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/touchpad_no.svg $color
	dunstify -h string:x-dunst-stack-tag:touchpad -u low -i "/tmp/icon.svg" "xinput" "Touchpad deactivated"
fi
