#!/bin/sh
# Change opacity gradually from alacritty

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/opacity.svg $color
# Get current value
current_opacity=$(grep 'opacity =' ~/.config/alacritty/alacritty.toml | awk ' {print $3} ')
# Change it accordingly to option selected
case $1 in
	"-i")
		echo "Relative change"
		new_opacity=$(printf '%3.1f\n' $(bc<<<$current_opacity+$2))
		#new_opacity=$(echo "$current_opacity + $2" | bc -l)
		if (( $(echo "$new_opacity < 0" | bc -l) )); then
			new_opacity="0"
		fi
		if (( $(echo "$new_opacity > 1" | bc -l) )); then
			new_opacity="1"
		fi
		echo $new_opacity
		sed -i --follow-symlinks "/opacity =/ s/$current_opacity/$new_opacity/" ~/.config/alacritty/alacritty.toml
		sed -i --follow-symlinks "/opacity =/ s/$current_opacity/$new_opacity/" ~/.config/alacritty/alacritty_np.toml
      dunstify -h string:x-dunst-stack-tag:opacity -u low -i '/tmp/icon.svg' "alacritty" "Opacity set to $new_opacity"
		;;
	"-a")
		echo "Absolute change"
		sed -i --follow-symlinks "/opacity =/ s/$current_opacity/$2/" ~/.config/alacritty/alacritty.toml
		sed -i --follow-symlinks "/opacity =/ s/$current_opacity/$2/" ~/.config/alacritty/alacritty_np.toml
      dunstify -h string:x-dunst-stack-tag:opacity -u low -i '/tmp/icon.svg' "alacritty" "Opacity set to $2"
		;;
esac
