#!/bin/sh
# Script that toggles dark/light mode

# Config variables
GTK_DARK_THEME="Gruvbox-Material-Dark"
GTK_LIGHT_THEME="Gruvbox-Light"
GTK_DARK_ICON_THEME="Gruvbox-Dark"
GTK_LIGHT_ICON_THEME="Gruvbox-Light"
ALACRITTY_DARK_THEME="gruvbox_material"
ALACRITTY_LIGHT_THEME="gruvbox_material_light"

# Get color for notification foreground
color=$(eval sh ~/.local/scripts/get_fg.sh -d)
# Get icon for notification and colorize
eval sh ~/.local/scripts/colorize_icon.sh ~/.local/icons/toggle_theme.svg $color

# Get current mode
current_theme="$(gsettings get org.gnome.desktop.interface color-scheme)"

if [ $current_theme == "'prefer-dark'" ]; then
    echo "Changing theme to light variation"

    # GTK theming
    sed -i --follow-symlinks "/\/ThemeName/ s/\".*\"/\"${GTK_LIGHT_THEME}\"/" $HOME/.config/xsettingsd/xsettingsd.conf
    sed -i --follow-symlinks "/IconThemeName/ s/\".*\"/\"${GTK_LIGHT_ICON_THEME}\"/" $HOME/.config/xsettingsd/xsettingsd.conf
    sed -i --follow-symlinks "/gtk-theme-name/ s/=.*/=${GTK_LIGHT_THEME}/" $HOME/.config/gtk-3.0/settings.ini
    sed -i --follow-symlinks "/gtk-icon-theme-name/ s/=.*/=${GTK_LIGHT_ICON_THEME}/" $HOME/.config/gtk-3.0/settings.ini
    xsettingsd &
    gsettings set org.gnome.desktop.interface color-scheme prefer-light
    sleep 0.2 && kill $!
    
    # Zathura theming
    darkLineNumber=$(grep -n "Dark theming" $HOME/.config/zathura/zathurarc | cut -d: -f 1)
    lightLineNumber=$(grep -n "Light theming" $HOME/.config/zathura/zathurarc | cut -d: -f 1)
    sed -i --follow-symlinks "$((darkLineNumber + 1))s/^/\"/" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((darkLineNumber + 2))s/^/\"/" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((lightLineNumber + 1))s/^\"//" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((lightLineNumber + 2))s/^\"//" $HOME/.config/zathura/zathurarc

    # Terminal theming
    sed -i --follow-symlinks "/colors:/ s/\*.*/\*${ALACRITTY_LIGHT_THEME}/" $HOME/.config/alacritty/alacritty.yml
    sed -i --follow-symlinks "/colors:/ s/\*.*/\*${ALACRITTY_LIGHT_THEME}/" $HOME/.config/alacritty/alacritty_np.yml
    sed -i --follow-symlinks "/opacity:/ s/: .*/: 1/" $HOME/.config/alacritty/alacritty.yml
    sed -i --follow-symlinks "/opacity:/ s/: .*/: 1/" $HOME/.config/alacritty/alacritty_np.yml
    sed -i --follow-symlinks "/set background/ s/=.*/=light/" $HOME/.vimrc

    # Picom shadows
    sed -i --follow-symlinks "/normal =/ s/#normal/normal/" $HOME/.config/picom/picom.conf

    # Send notification
    dunstify -h string:x-dunst-stack-tag:changeTheme \
    -i "/tmp/icon.svg" "System" "Changed to light theme"
else
    echo "Changing theme to dark variation"

    # GTK theming
    sed -i --follow-symlinks "/\/ThemeName/ s/\".*\"/\"${GTK_DARK_THEME}\"/" $HOME/.config/xsettingsd/xsettingsd.conf
    sed -i --follow-symlinks "/IconThemeName/ s/\".*\"/\"${GTK_DARK_ICON_THEME}\"/" $HOME/.config/xsettingsd/xsettingsd.conf
    sed -i --follow-symlinks "/gtk-theme-name/ s/=.*/=${GTK_DARK_THEME}/" $HOME/.config/gtk-3.0/settings.ini
    sed -i --follow-symlinks "/gtk-icon-theme-name/ s/=.*/=${GTK_DARK_ICON_THEME}/" $HOME/.config/gtk-3.0/settings.ini
    xsettingsd &
    gsettings set org.gnome.desktop.interface color-scheme prefer-dark
    sleep 0.2 && kill $!

    # Zathura theming
    darkLineNumber=$(grep -n "Dark theming" $HOME/.config/zathura/zathurarc | cut -d: -f 1)
    lightLineNumber=$(grep -n "Light theming" $HOME/.config/zathura/zathurarc | cut -d: -f 1)
    sed -i --follow-symlinks "$((lightLineNumber + 1))s/^/\"/" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((lightLineNumber + 2))s/^/\"/" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((darkLineNumber + 1))s/^\"//" $HOME/.config/zathura/zathurarc
    sed -i --follow-symlinks "$((darkLineNumber + 2))s/^\"//" $HOME/.config/zathura/zathurarc

    # Terminal theming
    sed -i --follow-symlinks "/colors:/ s/\*.*/\*${ALACRITTY_DARK_THEME}/" $HOME/.config/alacritty/alacritty.yml
    sed -i --follow-symlinks "/colors:/ s/\*.*/\*${ALACRITTY_DARK_THEME}/" $HOME/.config/alacritty/alacritty_np.yml
    sed -i --follow-symlinks "/opacity:/ s/: .*/: 0.8/" $HOME/.config/alacritty/alacritty.yml
    sed -i --follow-symlinks "/opacity:/ s/: .*/: 0.8/" $HOME/.config/alacritty/alacritty_np.yml
    sed -i --follow-symlinks "/set background/ s/=.*/=dark/" $HOME/.vimrc

    # Picom shadows
    sed -i --follow-symlinks "/normal =/ s/normal/#normal/" $HOME/.config/picom/picom.conf

    # Send notification
    dunstify -h string:x-dunst-stack-tag:changeTheme \
    -i "/tmp/icon.svg" "System" "Changed to dark theme"
fi
