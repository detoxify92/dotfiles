#!/bin/sh
# Scripts that launches the ncmpcpp music player

# Calls updater if it is not running
[[ -z $(pgrep mpdup) ]] && mpdup &

alacritty -e ncmpcpp
