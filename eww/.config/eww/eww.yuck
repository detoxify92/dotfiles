(defwindow bar
  :monitor 0
  :namespace "bar"
  :geometry (geometry :x "0%"
                      :y "1%"
                      :width "99%"
                      :height "36px"
                      :anchor "top center")
  :reserve (struts :side "top" :distance "40px")
  :exclusive true
  (bar))

  (defwidget bar []
    (centerbox :orientation "h"
    (music)
    (workspaces)
    (sidestuff)))

(defwidget music []
  (box :class "music"
    :orientation "h"
    :space-evenly false 
    :visible {music_status=='stopped' ? false : true}
    (image :path notification_cover
           :image-width "30"
           :image-height "30"
           :class "notification_cover")
    (blockMusic)
    (box :class "songInfo"
      :space-evenly false
      :orientation "v"
      :halign "start"
      (label :class "mpdsong"
        :halign "start"
        :text mpd_song
        :limit-width 60
      )
      (label :class "mpdalbum"
        :halign "start"
        :text mpd_album
        :limit-width 60
      )
    )
  )
)

(defwidget sidestuff []
  (box :class "sidestuff" 
    :orientation "h" 
    :space-evenly false 
    :halign "end"
      (system1)
      (system2)
      (system3)
      (timeDate)))

(defwidget blockMusic []
  (box :class "blockMusic"
    :orientation "v" 
    (mpd_info)
    (mpd_track)
  )
)

(defwidget system1 []
  (box :class "system1"
    :orientation "v" 
    :valign "center"
    :space-evenly false
    (cpu)
    (ram)
  )
)

(defwidget system2 []
  (box :class "system2"
    :orientation "v" 
    :valign "center"
    :space-evenly false
    (vol)
    (battery)
  )
)

(defwidget system3 []
  (box :class "system3"
    :valign "center"
    :space-evenly false
    :orientation "v" 
    (keyboard)
    (temp_digit)
  )
)

(defwidget timeDate []
  (box :class "timeDate"
    :orientation "v" 
    :valign "center"
    :space-evenly false
    (label :class "time" :text time)
    (label :class "date" :text date)
  )
)

(defwidget cpu []
  (box :class "cpu"
      (metric 
        :class "cpu_metric"
        :label ""
        :value {EWW_CPU.avg}
        :onchange "")
  )
)

(defwidget ram []
  (box :class "ram"
      (metric 
        :class "ram_metric"
        :label ""
        :value {EWW_RAM.used_mem_perc}
        :onchange "")
  )
)

(defwidget temp []
  (box :class "temp"
      (metric 
        :class "temp_metric"
        :label ""
        :value {EWW_TEMPS.CORETEMP_PACKAGE_ID_0}
        :onchange "")
  )
)

(defwidget temp_digit []
  (box 
      (label 
        :class {EWW_TEMPS.CORETEMP_PACKAGE_ID_0 > 70 ? "temp_digit warn" : "temp_digit"}
        :text " ${EWW_TEMPS.CORETEMP_PACKAGE_ID_0}"
      )
  )
)

(defwidget vol []
  (box :class "vol"
      (metric 
        :class {bar_volume == 0 ? "vol_metric_muted" : "vol_metric"}
        :label {bar_volume == 0 ? ''  : ''}
        :value bar_volume
        :onchange "amixer -D pulse sset Master {}%")
  )
)

(defwidget battery []
  (eventbox
    :onclick "bash ~/.config/eww/scripts/battery"
    (box 
    (metric 
      :class "battery_metric"
      :label {EWW_BATTERY=='' ? "" : "${EWW_BATTERY.BAT0.status == 'Charging' ? "" : ""}"}
      :value {EWW_BATTERY=='' ? 0 : EWW_BATTERY.BAT0.capacity}
      :onchange "")
      )
    )
  )

(defwidget keyboard []
  (label 
    :text " ${keyboard_layout} "
    :class "keyboard"     
  )
)

(defwidget mpd_track []
  (box :class "mpd_track"
    (metric 
      :class "music_metric"
      :label ""
      :value track
      :onchange "mpc seek {}%")
  )
)

(defwidget mpd_info []
  (label 
    :halign "center"
    :class "mpd_info"
    :text "${music_status == 'playing' ? '' : ''} ${music_random == 'on' ? '' : ''}")
)


(defwidget workspaces []
  (box :class "workspaces-widget"
    :space-evenly false
    :orientation "h"
    (eventbox :onscroll "bash ~/.config/eww/scripts/change-active-workspace {} ${current_workspace}" :class "workspaces"
      (box :space-evenly false
        (label :text "${workspaces}${current_workspace}" :visible false)
        (for workspace in workspaces
          (eventbox :onclick "hyprctl dispatch workspace ${workspace.id}"
            (box :class "workspace-entry-${workspace.id == current_workspace ? "current" : ""}-${workspace.windows > 0 ? "occupied" : "empty"}"
              (label :text "${workspace.id}")
            )
          )
        )
      )
    )
  )
)

(defwidget metric [class label value onchange]
(box :orientation "h"
     :class class
     :space-evenly false
  (box :class "label" label)
  (scale :min 0
         :max 101
         :active {onchange != ""}
         :value value
         :onchange onchange)))

(defpoll time :interval "1m"
  "date '+%H:%M'")

(defpoll date :interval "1m"
  "date '+%a, %b %d'")

(deflisten workspaces :initial "[]" "bash ~/.config/eww/scripts/get-workspaces")

(deflisten current_workspace :initial "1" "bash ~/.config/eww/scripts/get-active-workspace")

(defvar bar_volume "0")

(defvar mpd_song "")

(defvar mpd_album "")

(defpoll track :interval "2s"
                :initial "0" 
"scripts/track")

(defvar notification_cover "/tmp/notification_cover.jpg")

(defvar music_status "stopped")

(defvar music_random "off")

(defvar keyboard_layout "en")
