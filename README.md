# dotfiles

![screenshot](/img/screenshot.png?raw=true "screenshot")
![screenshot2](/img/screenshot2.png?raw=true "screenshot2")

These are my personal configuration files for various programs that I use.
These programs are normally used in a minimal setup which uses a window manager
instead a full-fledged desktop environment. To recreate a setup like the one
displayed in the screenshot, I recommend my builds for
[suckless](https://gitlab.com/detoxify92/suckless) software. These include:

- [alacritty](https://alacritty.org). A terminal emulator. It includes several
  colorschemes and a version with no padding, which I use to launch vifm.

- bin and scripts. The binaries are used mostly for the generation of the status
  bar with dwmblocks. The scripts main purpose is to aid in system usage.

- [dunst](https://dunst-project.org/). Lightweight notification daemon, very
  useful and customizable. 

- fontconfig. My font configuration. I use Ubuntu font family and the
  [fontawesome](https://fontawesome.com/) for many icons system wide. 

- icons. Several icons used mostly for the notifications.

- [mpd](https://www.musicpd.org/),
  [mpDris2](https://github.com/eonpatapon/mpDris2) and
  [ncmpcpp](https://www.musicpd.org/). The music tools I use. MPD is the music
  daemon, mpDris2 gives MPRIS 2 support to it, and ncmpcpp is the player
  itself.
 
- [newsboat](https://newsboat.org/). A RSS feed reader for the terminal.

- [picom](https://github.com/ibhagwan/picom). A X11 compositor with dual kawase blur.

- [vifm](https://vifm.info/). A terminal file manager with dual pane view and
  vim keybindings. The configuration include a colorscheme and unicode icons.

- [vim](https://vifm.info/). **THE** text editor. My configuration include some
  plugins and handy shortcuts.

- xprofile. Used to initialize some variables and start programs at startup.

- [zathura](https://pwmt.org/projects/zathura/). Minimalist document viewer. My
  configuration remaps some keybindings and set some colors.

- [zsh](https://zsh.sourceforge.io/). The shell that I use. My configuration
  just includes some colors for the prompt, sane defaults and aliases

## Installation and dotfiles deployment

Just clone the repository to a directory on your home. Then use [GNU
Stow](https://zsh.sourceforge.io/) to deploy the configuration wanted. For
instance, to deploy the configuration for alacritty do 

```bash
stow alacritty
```

Remember that there should be no file with the same name at the destination
folder or stow will complain. Also, I share these dotfiles across different
computers with different colorschemes
([everforest](https://gitlab.com/detoxify92/suckless) and
[gruvbox](https://github.com/morhetz/gruvbox)). Therefore, in some
configurations you may need to comment out or uncomment different colors for
the desired result.
