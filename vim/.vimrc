"      ___                       ___           ___           ___     
"     /\__\          ___        /\__\         /\  \         /\  \    
"    /:/  /         /\  \      /::|  |       /::\  \       /::\  \   
"   /:/  /          \:\  \    /:|:|  |      /:/\:\  \     /:/\:\  \  
"  /:/__/  ___      /::\__\  /:/|:|__|__   /::\~\:\  \   /:/  \:\  \ 
"  |:|  | /\__\  __/:/\/__/ /:/ |::::\__\ /:/\:\ \:\__\ /:/__/ \:\__\
"  |:|  |/:/  / /\/:/  /    \/__/~~/:/  / \/_|::\/:/  / \:\  \  \/__/
"  |:|__/:/  /  \::/__/           /:/  /     |:|::/  /   \:\  \      
"   \::::/__/    \:\__\          /:/  /      |:|\/__/     \:\  \     
"    ~~~~         \/__/         /:/  /       |:|  |        \:\__\    
"                               \/__/         \|__|         \/__/    
"
"
"
" Jaime's VIM configuration
" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Change the cursor for each mode: beam for insert, block for normal/visual
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[0 q"

" Show line numbering and highlight current line
set number
set cursorline
set relativenumber

" Share clipboard with X11
set clipboard=unnamedplus

" Search options
set incsearch
set hlsearch

" Scroll offset
set scrolloff=8

" Vimplug
call plug#begin('~/.vim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-latex/vim-latex'
Plug 'ap/vim-css-color'
Plug 'sainnhe/gruvbox-material'
Plug 'shinchu/lightline-gruvbox.vim'
Plug 'alvan/vim-closetag'
call plug#end()

" Vim theme

" Gruvbox material
let g:gruvbox_material_ui_contrast = 'high'
let g:gruvbox_material_background = 'medium'
let g:gruvbox_material_foreground = 'original'
let g:lightline = {'colorscheme' : 'gruvbox_material'}
let g:gruvbox_material_enable_bold = 1
let g:gruvbox_material_better_performance = 1
if has("gui_running")
   let g:gruvbox_material_transparent_background = 0
   set go -=m
   set go -=T
   set go -=r
   set go -=L
   set guifont=Ubuntu\ Mono\ 12
   let g:gruvbox_material_visual = 'green background'
   set belloff=all
else
   let g:gruvbox_material_transparent_background = 1
   let g:gruvbox_material_visual = 'green background'
endif

set termguicolors
set laststatus=2       " Needed for lighline
set background=dark    " Setting dark mode
colorscheme gruvbox-material

" Remove insert message when using lighline
set noshowmode

" Syntax highlight
:filetype plugin on
:filetype indent on
:syntax on

" Indenting
set autoindent
set tabstop=8
set shiftwidth=4
set softtabstop=4
set expandtab

" Latex
let g:tex_flavor='latex'

" Splits
set splitbelow splitright

" Leader key
let mapleader = " "

" Remapings
" Alternate between open panes with tab
nnoremap <tab> <c-w>w

" Ctrl+c as escape (Ctrl+[ also does the trick)
inoremap <C-c> <ESC>
vnoremap <C-c> <ESC>

" NerdTree
map <C-n> :NERDTreeToggle<CR>

" Yank everything to + register
nnoremap ya :%y+<CR>

" Yank up to the last character of the line
nnoremap Y yg_

" Shortcuts for deleting, changing, selecting inside several enclosures 
onoremap p i(
onoremap s i[
onoremap c i{

" Delete one character or selection without copying to register
nnoremap x "_x
nnoremap <leader>x x
vnoremap <leader>d "_d

" Move selection up or down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Move line up or down
nnoremap <leader>k :m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==

" Get rid of the last search hightlighting
nnoremap <silent> <leader>n :nohlsearch<Bar>:echo<CR>

" Keep it centered while searching
nnoremap n nzzzv
nnoremap N Nzzzv

" Set some sane undo breakpoints
inoremap <space> <space><c-g>u

" Tab navigation
nnoremap gp :tabprev<CR>
